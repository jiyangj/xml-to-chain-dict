import xmltocd

s = """
    <root id='root'>
        <a id="a1">123</a>
        <b></b>
        <b></b>
        <b></b>
        <c>hello world</c>
    </root>
"""
xmlManager = xmltocd.parse_string(s, real_cdata_key='string')

import json
# print(json.dumps(xmlManager.xml, indent=4))

node = xmlManager.find_node_by_attrs(id='root')
xmlManager.add_attrs(node, ('name', 'jiyang'))
xmlManager.batch_update_attrs(node, name='qlzqlz')
xmlManager.update_text(node, '1234567890')

node_a = xmlManager.find_node_by_attrs(id='a1')
xmlManager.update_text(node_a, 'node_a')

insert_node = xmlManager.insert(node, tag='table', attrs={'style':'color: black;'}, text='html')
print(insert_node.string)
print(xmlManager.objects.get_tag(insert_node))

# xmlManager._id_nodes[xmlManager.objects.parent(id(node.b[0]))]
# [OrderedDict([('string', '')]), OrderedDict([('string', '')]), OrderedDict([('string', '')])]

xmlManager.add_attrs(node.b[0], id='test')

print(node.b[0].id)
print(node.b[1].string)

insert_node = xmlManager.insert(node.b, attrs={'id':'new_b'}, text='bbbbbbbb')
print(insert_node.string)
print(xmlManager.objects.get_tag(insert_node))

# print(json.dumps(xmlManager.xml, indent=4))

print(node.c)

# print(node.name)
xmlManager.save()
