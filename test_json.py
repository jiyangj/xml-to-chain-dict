import xmltocd

json_data = """
{
    "root": {
        "todos": [
            {
                "title": "todo1",
                "undo": false,
                "content": "something has been happened."
            },
            {
                "title": "todo2",
                "undo": true,
                "content": "oh oh oh"
            }
        ],
        "datas": [
            12,
            13,
            14
        ]
    }
}
"""

jsonManager = xmltocd.parse_json(json_data)
todos = jsonManager.find_nodes_by_tag("todos")
# print(jsonManager.get_parent(todos[0]))
print(todos[1].title) # todo2
print(todos[1].undo) # True
print(todos[1].content) # oh oh oh

datas = jsonManager.find_nodes_by_tag('datas')

jsonManager.get_parent(datas[0])

print([jsonManager.find_text(_) for _ in datas])
