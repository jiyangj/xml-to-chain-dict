import xmltocd

s = """
    <root id='root'>
        78946546
        <a id="a1">123</a>
        <a>1234</a>
        <a>1235</a>
        <b name='bb'>
            <c id='c1' okk='yiu'>okk</c>
            <d json='{"x": 888}' e='eee'>{"x": 666}
                <e>789789789</e>
            </d>
        </b>
        <e2></e2>
        6789
    </root>
"""
xmlManager = xmltocd.parse_string(s, real_cdata_key='string')
import json
# print(json.dumps(xmlManager.objects.doc, indent=4))
# print(json.dumps(xmlManager.xml, indent=4))

print(xmlManager.find_nodes_by_tag('e2', one_=True).string)

node = xmlManager.find_nodes_with_descendants(
    constraint={
        'args': []
        ,'kwargs': {'tag_': "e"}
    }
    , tag_='d'
    , one_=True
)
print(node.json)


# # xmlManager.pop_node_by_attrs(name='bb')
# # print(json.dumps(xmlManager.xml, indent=4))

# ns = xmlManager.find_node_by_attrs(id='root')
# print(type(ns.a))
