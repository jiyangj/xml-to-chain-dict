import xmltocd

xmlManager = xmltocd.parse('ttt.xml', real_cdata_key='string')

# help_node = xmlManager.find_node_by_attrs(id="城建税、教育费附加、地方教育附加税（费）申报表")
# help_nodes = xmlManager.find_nodes_by_tag('TaskItem')

# import json
# # print(json.dumps(help_nodes[-1], indent=4, ensure_ascii=False))
# BlankCells = xmlManager.find_nodes_with_ancestor(help_node, tag_='BlankCell')
# print(BlankCells)

# print(BlankCells[-1].string)
# print(len(BlankCells))
# print(json.dumps(BlankCells[0], indent=4, ensure_ascii=False))

# print(xmlManager.find_nodes_by_tag('BlankCell')[-1].string)


# print(xmlManager.find_nodes_by_tag('BlankCell')[-27].string)
# print(xmlManager.find_nodes_by_tag_and_attrs('BlankCell')[-27].string)

# obj = xmlManager.find_nodes_by_text('甘肃融辉盛源商贸有限公司')[0]
# print(obj.tax)
# print(xmlManager.objects.get_tag(obj))
# print(xmlManager.objects.get_tag(xmlManager.find_node_by_attrs(id="城建税、教育费附加、地方教育附加税（费）申报表")))
# print(xmlManager.objects.get_tag(xmlManager.find_node_by_attrs(serialNo="871309828087238657")))


# print(xmlManager.find_nodes_by_tag_and_attrs('BlankCell', id="031_8_8")[0].tax)


# print(len(xmlManager.find_nodes_by_attrs(id="zzs-ybzzs")))


# node = xmlManager.find_nodes_by_tag_text_attrs(text='0.00', tag='BlankCell', id='001_14_6', one_=True)
# # print(node)
# # print(node.tax)
# # print(xmlManager.objects.get_tag(node))


node = xmlManager.find_node_by_attrs(id="GANSU_ZZS_YBNSR")
print(node.ssqs, node.ssqz, node.string)

help_node = xmlManager.find_node_by_attrs(id="增值税纳税申报表（一般纳税人适用）")
node = xmlManager.find_nodes_with_ancestor(help_node, tag_='TaskParam', one_=True)
cells = xmlManager.objects.get_children(node)
print(len(cells))
# print(cells[0].tax)

node = xmlManager.find_nodes_with_sibling_ancestor(
    constraint={
        'args': []
        ,'kwargs': {'id': "testtesttesttesttest"}
    }
    , tag_='TaskParam'
    , one_=True
)
cells = xmlManager.objects.get_children(node)
print(len(cells))


# BlankCells = xmlManager.find_nodes_with_ancestor(help_node, tag='BlankCell')
# print(len(BlankCells))
# print(len(node.VerifyCell))
# print(BlankCells[2].tax)
