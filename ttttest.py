from typing import List
import xmltocd
from xmltocd import ChainManager

xml_data = """\
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
666
   <a id='a'>
   1
      <b>
      2
         <dd>3</dd>
      </b>
      <b>
      4
         <dd>5</dd>
      </b>
   </a>
</kml>
"""

xmlManager: ChainManager = xmltocd.parse_string(xml_data)
node = xmlManager.find_nodes_by_tag('a', one_=True)
xmlManager.popitem(node)

xmlManager.save('output2.xml')
