import xmltocd

dict_data = {
    "root": {
        "todos": [
            {
                "title": "todo1",
                "undo": False,
                "content": "something has been happened."
            },
            {
                "title": "todo2",
                "undo": True,
                "content": "oh oh oh"
            }
        ],
        "datas": [
            12,
            13,
            14
        ]
    }
}

dictManager = xmltocd.parse_dict(dict_data)
# todos = dictManager.find_nodes_by_tag("todos")
# # print(dictManager.get_parent(todos[0]))
# print(todos[1].title) # todo2
# print(todos[1].undo) # True
# print(todos[1].content) # oh oh oh

# datas = dictManager.find_nodes_by_tag('datas')

# dictManager.get_parent(datas[0])

# print([dictManager.find_text(_) for _ in datas])


nodes = dictManager.find_nodes_by_text(12)
print(dictManager.get_tag(nodes[0]))

nodes = dictManager.find_nodes_by_tag('title')
print([dictManager.find_text(_) for _ in nodes])
